//
//  ViewController.swift
//  RetraceApp
//
//  Created by Ishan  Malhotra on 27/06/17.
//  Copyright © 2017 Ishan  Malhotra. All rights reserved.
//

import UIKit
import GoogleMaps



class ViewController: UIViewController,CLLocationManagerDelegate {
  
    
    var map:GMSMapView?
    var directGps = CLLocationManager()
    var path_P = GMSMutablePath()
    var line = GMSPolyline()
    var old = CLLocationCoordinate2D()
    var new = CLLocationCoordinate2D()
    var flag = false;
    var flagForPause = false;
    @IBOutlet var buttonForPause:UIButton?
    @IBOutlet var viewForMap:UIView?
    @IBOutlet var buttonForStop: UIButton?
    var destination = CLLocationCoordinate2D()
    var startReadingLocation = false;
    @IBOutlet var lblDistance: UILabel?
    @IBOutlet var buttonToDropPin: UIButton?
    var distanceDiffrence:Double? = 0
    var oldLatDistance:Double? = 0
    var oldLongDistance:Double? = 0
    var newLatDistance:Double? = 0
    var newLongDistance:Double? = 0
    var oldLat:Int32? = 0
    var avgLatOld:Int = 0
    var avgLongOld:Int = 0
    var avgLatNew:Int = 0
    var avgLongNew:Int = 0
    var flagForBorders = false;
    var alertPopUp:UIAlertController?

    

    var time:Timer?
    
    
    override func viewDidLoad() {

        super.viewDidLoad()
        let camera = GMSCameraPosition.camera(withLatitude: -15, longitude: 45, zoom: 1)
        map = GMSMapView.map(withFrame:(viewForMap?.bounds)!, camera: camera)
        map?.isMyLocationEnabled = true;
        map?.settings.zoomGestures = false;
        viewForMap?.addSubview(map!)
        directGps.delegate = self
        directGps.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        directGps.distanceFilter = kCLDistanceFilterNone
        directGps.requestLocation()
        directGps.requestAlwaysAuthorization()
        directGps.startUpdatingLocation();
        startReadingLocation = true;
        self.directGps.allowsBackgroundLocationUpdates = true
        self.becomeFirstResponder()
         alertPopUp = UIAlertController(title:"Location Update" , message: "Shake device to move to current geographical location", preferredStyle:.alert)
        
        //self.present(alertPopUp!, animated: true, completion:nil)


       // Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(ViewController.dismisPopup), userInfo: nil, repeats: false)
        
        //  Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(ViewController.startLocation), userInfo: nil, repeats: false)
        
        //print ("helo")
        
        
        //abc()
        // Do any additional setup after loading the view, typically from a nib.
    }
    func dismisPopup(){
        alertPopUp?.dismiss(animated: true, completion: nil)
    }
    func startLocation() {
        time = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(ViewController.timeCalled), userInfo: nil, repeats: true)
        

    }
    
    
    func addGoogleMap(La:CLLocationDegrees,lo:CLLocationDegrees ) {
        
        
        
        // Creates a marker in the center of the map.
        
        let camera = GMSCameraPosition.camera(withLatitude: La, longitude: lo, zoom: 18)
        map?.animate(to: camera)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let coordinate = locations[0].coordinate
       // new = coordinate
        
        print("backgroundcheck",coordinate)
        
        if startReadingLocation{
        
       print ( new.latitude)
        if !flag {
            old = coordinate;
            flag = !flag
            print("old ",old)
            addGoogleMap(La: old.latitude, lo: old.longitude)
      
        }
        else {
            new = coordinate;
            print("new ",new)
            newLatDistance = new.latitude * pow(10, 4)
            newLongDistance = new.longitude * pow(10, 4)
            print("newLatDistance ",Int((newLatDistance?.rounded())!))
            print("newLongDistance ",Int((newLongDistance?.rounded())!))
            avgLatNew = Int((newLatDistance?.rounded())!)
            avgLongNew = Int((newLongDistance?.rounded())!)
            
        }
        
    
        if buttonForStop?.isEnabled == false && buttonToDropPin?.isEnabled == false {
//            if abs((new.latitude-old.latitude)) < 0.000012 && abs(new.longitude-old.longitude) < 0.000012 {
//            showDestinatationReached()
//             //   print ( "new" new.latitude)
//                print ("abs", abs((new.latitude-old.latitude)))
//                
//            }
            
//          let distanceNew = CLLocation(latitude: new.latitude, longitude: new.longitude)
//        let distanceOld = CLLocation(latitude: old.latitude, longitude: old.longitude)
//            let finalDistance = distanceOld.distance(from: distanceNew)
//            lblDistance?.text = String(finalDistance)
//            distanceDiffrence =
            
            
//            if finalDistance < 2 {
//                showDestinatationReached()
//
//                
//            }
            
            lblDistance?.text = (String(abs(avgLongOld-avgLongNew))) + ",," + (String(abs(avgLatOld-avgLatNew)))
            if abs(avgLongOld-avgLongNew) < 2 && abs(avgLatOld-avgLatNew) < 2 {
                showDestinatationReached()

            }
       }
           
            
        }
        
//        print("absolute = ",abs((new.latitude-old.latitude)))
//        
//        if (abs((new.latitude-old.latitude)) > 0.000005) || abs(new.longitude-old.longitude) > 0.000005
//        {
//            print(new)
//
//        path_P.add(coordinate)
//        line.path = path_P;
//        line.strokeColor = UIColor.red;
//        line.strokeWidth = 5.0
//        line.map = map;
//        old.latitude=new.latitude;
//            
//        }
        
        
        
    }
    func timeCalled() {
        if abs((new.latitude-old.latitude)) > 0.00005 || abs(new.longitude-old.longitude) > 0.00005
        {
            if !(abs((new.latitude-old.latitude)) > 10) {
            print("new timer called ",new)
            
            path_P.add(new)
            line.path = path_P;
            line.strokeColor = UIColor.red;
            line.strokeWidth = 5.0
            line.map = map;
            old = new;
            }
            
        }
//        print(new)
//        path_P.add(new)
//        line.path = path_P;
//        line.strokeColor = UIColor.red;
//        line.strokeWidth = 5.0
//        line.map = map;
//        old.latitude=new.latitude;
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func toQuitTrip(){
        //        directGps.stopUpdatingLocation(). To Not stop the blue dot from giving current locations
        
        flagForBorders = false
        forSettingBorders(borderName: buttonForStop!)

        
        if buttonForPause?.isEnabled == true {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude:new.latitude,longitude: new.longitude)

        
        //        marker.title = "Sydney"
        //        marker.snippet = "Australia"
        marker.map = map
            map?.selectedMarker = marker
            marker.icon = GMSMarker.markerImage(with: UIColor.green)
        }
        time?.invalidate()
       buttonForStop?.setTitleColor(.red, for: .normal)
        buttonForPause?.isEnabled = false;
        
        alertSystem();
        

        
//        let transformForMap = CGAffineTransform()
//        transformForMap.rotated(by: 0)
//        viewForMap?.transform = transformForMap
        
       
    

        
    }
    
    @IBAction func toPauseTrip(){

        if flagForPause {
        directGps.stopUpdatingLocation()
        buttonForPause?.setTitleColor(.red, for: .normal)

        }
        else {
            flagForBorders = false

            directGps.startUpdatingLocation()
            buttonForPause?.setTitleColor(.green, for: .normal)

        }
        flagForPause = !flagForPause
        forSettingBorders(borderName:buttonForPause! )

    }
    
    func alertSystem(){
        
        let popForQuitingTrip = UIAlertController(title: "Return Trip", message: "Do you wish to return to your innitial location", preferredStyle: .alert)
        let actionForPop = UIAlertAction(title: "Yes", style: .default, handler: {(alert:UIAlertAction) in
            
            let camera = GMSCameraPosition.camera(withLatitude:self.new.latitude, longitude: self.new.longitude, zoom: 17, bearing:180, viewingAngle: 0)
            
            self.map?.animate(to: camera);
            self.buttonForStop?.isEnabled = false;
            self.destination = self.new
            
        })
        let actionForPopNo = UIAlertAction(title: "No", style: .default, handler: {(alert:UIAlertAction) in
            
            
        })
        popForQuitingTrip.addAction(actionForPop)
        
        popForQuitingTrip.addAction(actionForPopNo)
        self.present(popForQuitingTrip, animated: true, completion: nil);
        

        
    }
    
    func showDestinatationReached(){
        let popForReachingDestination = UIAlertController(title: "Trip Update", message: "You have correctly reached your location", preferredStyle: .alert)
        let actionForOk = UIAlertAction(title: "Ok", style: .cancel, handler: {(alert:UIAlertAction) in
            })
        popForReachingDestination.addAction(actionForOk)
        self.present(popForReachingDestination, animated: true, completion: nil);
        self.directGps.stopUpdatingLocation()
        
    }
    
    @IBAction func forDropingLocationMark(){
        forSettingBorders(borderName:buttonToDropPin!)
        old = new;
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: old.latitude, longitude: old.longitude)
        //        marker.title = "Sydney"
        //        marker.snippet = "Australia"
        marker.map = map
        
        
        
        path_P.add(old)
        oldLatDistance = old.latitude * pow(10, 4)
        oldLongDistance = old.longitude * pow(10, 4)
        print("oldLatDistance ",Int((oldLatDistance?.rounded())!))
        print("oldLongDistance ",Int((oldLongDistance?.rounded())!))
        avgLatOld = Int((oldLatDistance?.rounded())!)
        avgLongOld = Int((oldLongDistance?.rounded())!)
        
        buttonToDropPin?.isEnabled = false;
        buttonForStop?.isEnabled = true
        buttonForPause?.isEnabled = true

        startLocation()
        
    }
    func forSettingBorders(borderName:UIButton){
        borderName.layer.borderWidth = 1.0

        if !flagForBorders {
   
        borderName.layer.borderColor = UIColor.white.cgColor
        }
        else {
            borderName.layer.borderColor = UIColor.black.cgColor

        }
        flagForBorders = !flagForBorders
        
        
    }
    
//    override func canBecomeFirstResponder() -> Bool {
//        return true
//    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if(event?.subtype == UIEventSubtype.motionShake) {
            print("You shook me, now what")
            let camera = GMSCameraPosition.camera(withLatitude: new.latitude, longitude:new.longitude, zoom: 18)
            map?.animate(to: camera)
        }
    }
    


}

