//
//  StartAppPage.swift
//  MapTest
//
//  Created by Ishan  Malhotra on 27/06/17.
//  Copyright © 2017 Ishan  Malhotra. All rights reserved.
//

import UIKit

class StartAppPage: UIViewController {
  @IBOutlet var buttonForMap: UIButton?
    @IBOutlet var buttonForHowToUse: UIButton?
    @IBOutlet var LableForName: UILabel?
    @IBOutlet var buttonForAboutTheApp: UIButton?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonForMap?.layer.borderWidth = 2.0
        buttonForMap?.layer.borderColor = UIColor.white.cgColor
        // Do any additional setup after loading the view.
    }
    @IBAction func startTripCalled(){
        let vw = ViewController();
        navigationController?.pushViewController(vw, animated: true);
        
    }

    @IBAction func toShowHowToUsePage(){
        let HowToUsePage = HowToUse()
        navigationController?.pushViewController(HowToUsePage, animated: true)
        
        
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func toShowAboutTheAppPage(){
        let aboutTheApp = AboutTheApp()
        navigationController?.pushViewController(aboutTheApp, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
